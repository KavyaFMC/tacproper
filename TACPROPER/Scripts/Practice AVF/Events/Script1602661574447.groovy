import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions

Properties prop3 = HelperFunctions.getTheObject('C:\\Users\\z00485076\\Katalon Studio\\TACPROPER\\DataBase\\Event.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('event'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('addnewevent'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('affectedAccess'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('selectaffectedAccess'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('observedBy'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('selectobservedBy'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('observedduring'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('observedduringselect'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('date'), '10/14/2020', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('finding1'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('finding2'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('movetoright'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('next'), '', 'click')

WebUI.delay(3)

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('next1'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('finding3'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('findingssave'), '', 'click')

WebUI.delay(2)
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('pencilbutton'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('editevent'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('changetoerroneous'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('addintervention'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('intervention'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('selectintervention'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('interventiondate'), '10/14/2020', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('textarea'), 'Text123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('saveintervention'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('yes'), '', 'click')

WebUI.delay(2)

WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('deleate'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('deleatenotes'), 'Notes123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('eventdeleate'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

