import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions
Properties prop2=HelperFunctions.getTheObject('C:\\Users\\z00485076\\Katalon Studio\\TACPROPER\\DataBase\\Appointment.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentclick'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('addappointment'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentaffectedaccess'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('selectavfappointment'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('providerappointment'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('providerselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentreason'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('reasonforappointment'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('choosethetype'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentedit'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentdate'),'10/01/2020','input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('hour'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('hourselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('min'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('minselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('pm'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('pmselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentnotes'),'notes123','input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('datesave'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('arrow'),'','click')
WebUI.delay(3)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appoitmentoutcome'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appoitmentedit'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('apptstatus'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('apptcomplete'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('procedureperformed'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('procedureperformeddropdown'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('outcomenotes'),'Text123','input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('outcomesave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
















