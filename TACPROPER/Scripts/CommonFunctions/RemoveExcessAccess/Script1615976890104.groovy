import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import helperUtilities.HelperFunctions as HelperFunctions
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver

//Properties prop = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVF\\Tac.Properties')
Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\CommonFunctions\\EditAccess.properties')

//System.setProperty("webdriver.chrome.driver","C:\\Users\\z00445927\\Downloads\\Katalon_Studio_Windows_64-7.1.2\\Katalon_Studio_Windows_64-7.1.2\\configuration\\resources\\drivers\\chromedriver_win32\\chromedriver.exe")
WebUI.openBrowser('http://dama2.qa-intranet.fmcna.com/tac-app')

WebUI.openBrowser(GlobalVariable.URL)
WebDriver driver = DriverFactory.getWebDriver()

WebUI.waitForPageLoad(5)

WebUI.maximizeWindow()

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), 'tnurse', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), 'Today123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('signinbutton'), '', 'click')

WebUI.waitForPageLoad(5)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 
    'AVF', true)

//def Clinic=excelData.getValue("Clinic", 1)
def Clinic = '6300'

println(Clinic)

def MRN = '5000602883'

//def MRN=excelData.getValue("MRN", 1)
println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectclinic'), Clinic, 'input')

WebUI.delay(2)

Robot rb = new Robot()

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient'), MRN, 'input')

WebUI.delay(3)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient1'), '', 'click')

WebUI.delay(3)

List<WebElement> editBtn = driver.findElements(By.xpath(prop.getProperty('pencil')))

def totalAccess = editBtn.size()

println('Total Access' + totalAccess)

//-- Permanently unusable count Check--
//WebUI.setText(prop.getProperty('status'),"Active")
driver.findElement(By.xpath(prop.getProperty('status'))).sendKeys('Permanently')

List<WebElement> puelements = driver.findElements(By.xpath(prop.getProperty('pencil')))

def puCount = puelements.size()

//-- Permanently unusable count Check--
//WebUI.setText(prop.getProperty('status'),"Active")
driver.findElement(By.xpath(prop.getProperty('status'))).clear()

driver.findElement(By.xpath(prop.getProperty('status'))).sendKeys('Removed')

List<WebElement> removedeelments = driver.findElements(By.xpath(prop.getProperty('pencil')))

def removedCount = removedeelments.size()

//-- Permanently unusable count Check--
//WebUI.setText(prop.getProperty('status'),"Active")
driver.findElement(By.xpath(prop.getProperty('status'))).clear()

List<WebElement> activeinuse = driver.findElements(By.xpath(prop.getProperty('pencil')))

def activeinusecount = activeinuse.size()

int validCount = 0

println('PU' + puCount)

println('Removed' + removedCount)

println('Active' + activeinusecount)

int tempCount = puCount + removedCount

if (tempCount >= activeinusecount) {
    validCount = (tempCount - activeinusecount)

    println('valid' + validCount)
} else {
    validCount = (activeinusecount - tempCount)

    println('valid' + validCount)
}

if (validCount > 2) {
    CustomKeywords.'pageLocators.PageAction.accessRemoval'(activeinuse, validCount)
} else {
    println('This patient does not having excess access!!!')
}

