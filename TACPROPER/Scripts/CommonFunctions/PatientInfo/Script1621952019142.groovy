import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys

Properties prop  = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\PatientInfo.Properties')

WebDriver driver=DriverFactory.getWebDriver();
JavascriptExecutor js = (JavascriptExecutor) driver
//-----------------------Hilighting Policy Information in Add Access Tab------------------------
Company = driver.findElement(By.xpath(prop.getProperty('Company')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Company)

TestObject testObj = new TestObject()
def a = testObj.addProperty('xpath', ConditionType.EQUALS,  prop.getProperty('PlanName'))

if(WebUI.waitForElementPresent(a, 2, FailureHandling.OPTIONAL)) {

	PlanName = driver.findElement(By.xpath(prop.getProperty('PlanName')))
	//PlanText=driver.findElement(By.xpath("//p[contains(.,'Plan Name')]")).getAttribute("textContent")
	js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",PlanName)

	PolicyGroup = driver.findElement(By.xpath(prop.getProperty('PolicyGroup')))
	js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",PolicyGroup)

	Phone = driver.findElement(By.xpath(prop.getProperty('Phone')))
	js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Phone)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Company'), null, 'scrolltoelement')

}else {
	println('do-nothing')
}


Company = driver.findElement(By.xpath(prop.getProperty('Kt')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Company)

WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('audit'), null, 'scrolltoelement')
