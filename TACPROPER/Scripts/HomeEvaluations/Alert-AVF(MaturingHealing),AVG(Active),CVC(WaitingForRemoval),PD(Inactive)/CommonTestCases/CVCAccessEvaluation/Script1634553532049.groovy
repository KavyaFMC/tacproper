import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys
import org.openqa.selenium.WebElement

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')

//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('HomeEvalBtn'), '', 'click')
WebUI.delay(2)

TestObject testObj =  new TestObject()
WebDriver driver = DriverFactory.getWebDriver()

//========================AVG Access Evaluation==============================
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
String CVC_TodayEval = excelData.getValue("Today's Evaluation", 2)
String CVC_Last30D = excelData.getValue("Last 30 Days", 2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcHomeDropDown'), '', 'click')
WebUI.delay(2)
println prop.getProperty('cvcAccessEval')


//''''''''''Verify CVC Access Evaluation Disabled''''''''''''''''''''

String cvcEvaldisable = prop.getProperty('cvcAccessEval')+prop.getProperty('GrayedoutPlus')

def accessEval = testObj.addProperty('xpath', ConditionType.EQUALS, cvcEvaldisable)
if(WebUI.waitForElementPresent(accessEval, 2, FailureHandling.OPTIONAL)) {
	println "AVG Access Evaluation Cannot be Performed"
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcAccessEval'), '', 'click')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcHomeDropDown'), '', 'click')
}else {
		
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcAccessEval'), '', 'click')
	WebUI.takeScreenshot()
	
	String accEvalEnable = prop.getProperty('cvcAccessEval')+prop.getProperty('EnablePlus')
	CustomKeywords.'customLocators.FindElement.pageAction'(accEvalEnable, '', 'click')
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TeleHealth'), '', 'click')
	
	//---------------Look Findings-------------------
	String[] Today_Findings = CVC_TodayEval.split(",")
	for(int i=0;i<Today_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TodayEval')+Today_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	//---------------Findings In Last 30 Days----------------------------
	String[] Last30D_Findings = CVC_Last30D.split(",")
	for(int i=0;i<Last30D_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Past_30Days')+Last30D_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	WebUI.takeScreenshot()
	WebUI.delay(2)
	
	def nextBtn = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('next'))
	if(WebUI.waitForElementPresent(nextBtn, 2, FailureHandling.OPTIONAL)) {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), '', 'click')
		WebUI.delay(2)
		
		String hometIntevention = excelData.getValue("HomeIntervention", 2)
		List<WebElement> ele = driver.findElements(By.xpath(prop.getProperty('intervention')+hometIntevention+prop.getProperty('brace')))
		
		println ele.size()
		for(WebElement intervention : ele) {
			intervention.click()
			WebUI.delay(1)
		}
		WebUI.takeScreenshot()
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}else {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}
	
	WebUI.delay(2)
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcHomeDropDown'), '', 'click')
	WebUI.delay(2)
	println prop.getProperty('cvcAccessEval')
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcAccessEval'), '', 'click')
	WebUI.delay(2)
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcAccessEval')+prop.getProperty('ViewEvaluation'), '', 'click')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'scrolltoelement')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'click')
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcAccessEval'), '', 'click')
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcHomeDropDown'), '', 'click')
}

