import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys
import org.openqa.selenium.WebElement

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')


WebUI.delay(2)

TestObject testObj =  new TestObject()

WebDriver driver = DriverFactory.getWebDriver()

//========================AVF Maturation Evaluation==============================
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVF', true)
String AVF_Look = excelData.getValue("Maturation Look", 2)
String AVF_Listen = excelData.getValue("Maturation Listen", 2)
String AVF_Feel = excelData.getValue("Maturation Feel", 2)


WebUI.delay(2)
println prop.getProperty('avfMaturationEval')

//''''''''''Verify AVF Maturation Evaluation Disabled''''''''''''''''''''

String matEvaldisable = prop.getProperty('avfMaturationEval')+prop.getProperty('GrayedoutPlus')

def maturationEval = testObj.addProperty('xpath', ConditionType.EQUALS, matEvaldisable)
if(WebUI.waitForElementPresent(maturationEval, 2, FailureHandling.OPTIONAL)) {
	
	println "AVF Access Evaluation Cannot be Performed"
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfMaturationEval'), '', 'click')
	WebUI.delay(1)
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfMaturationEval'), '', 'click')
	WebUI.delay(1)
	//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfHomeDropDown'), '', 'click')
	
}else {
		
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfMaturationEval'), '', 'click')
	WebUI.takeScreenshot()
	
	String matEvalEnable = prop.getProperty('avfMaturationEval')+prop.getProperty('EnablePlus')
	CustomKeywords.'customLocators.FindElement.pageAction'(matEvalEnable, '', 'click')
	WebUI.delay(2)
	
	//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TeleHealth'), '', 'click')
	
	//---------------Look Findings-------------------
	String[] Look_Findings = AVF_Look.split(",")
	for(int i=0;i<Look_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Maturation_Look')+Look_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	//---------------Listen Findings----------------------------
	String[] Listen_Findings = AVF_Listen.split(";")
	for(int i=0;i<Listen_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Maturation_Listen')+Listen_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), null, 'scrolltoelement')
	
	//---------------Feel Findings----------------------------
	String[] Feel_Findings = AVF_Feel.split(",")
	for(int i=0;i<Feel_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Maturation_Feel')+Feel_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	WebUI.takeScreenshot()
	
	def nextBtn = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('next'))
	if(WebUI.waitForElementPresent(nextBtn, 2, FailureHandling.OPTIONAL)) {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), '', 'click')
		WebUI.delay(2)
		
		String hometIntevention = excelData.getValue("HomeIntervention", 2)
		List<WebElement> ele = driver.findElements(By.xpath(prop.getProperty('intervention')+hometIntevention+prop.getProperty('brace')))
		
		println ele.size()
		for(WebElement intervention : ele) {
			intervention.click()
			WebUI.delay(1)
		}
		WebUI.takeScreenshot()
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}else {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}
	
	WebUI.delay(3)
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfHomeDropDown'), '', 'click')
	WebUI.delay(2)
	println prop.getProperty('avfMaturationEval')
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfMaturationEval'), '', 'click')
	WebUI.delay(2)
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfMaturationEval')+prop.getProperty('ViewEvaluation'), '', 'click')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'scrolltoelement')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'click')
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avfMaturationEval'), '', 'click')
}

