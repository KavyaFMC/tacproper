import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys
import org.openqa.selenium.WebElement

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')

//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('HomeEvalBtn'), '', 'click')
WebUI.delay(2)

TestObject testObj =  new TestObject()

WebDriver driver = DriverFactory.getWebDriver()
//JavascriptExecutor js = (JavascriptExecutor) driver

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PD', true)
String Peritonitis_Findings = excelData.getValue("Symptoms of Peritonitis", 4)
String ExitSite_Findings = excelData.getValue("Exit Site Evaluation", 4)
String TunnelEval_Findings = excelData.getValue("Tunnel Evaluation", 4)
String PDEval_Findings = excelData.getValue("PD Catheter Evaluation", 4)


//Modality = driver.findElement(By.xpath(prop.getProperty('Modality')))
//js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Modality)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdHomeDropDown'), '', 'click')
WebUI.delay(1)



def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('GrayedoutPlus')+1+']')
if(WebUI.waitForElementPresent(a, 2, FailureHandling.OPTIONAL)) {
	println "Cannot Perform Access Evaluation"
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccessEval'), '', 'click')
	WebUI.delay(2)
	WebUI.takeScreenshot()
}else {
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccessEval'), '', 'click')
	WebUI.delay(1)
	WebUI.takeScreenshot()
	
	String accEvalEnable = prop.getProperty('pdAccessEval')+prop.getProperty('EnablePlus')
	CustomKeywords.'customLocators.FindElement.pageAction'(accEvalEnable, '', 'click')
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TeleHealth'), '', 'click')
	
//-------------------------Symptoms Of Peritonitis Findings-----------------------
	String[] Peri_Findings = Peritonitis_Findings.split(",")
	for(int i=0;i<Peri_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Peritonitis')+Peri_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)	
	}
	
	
//-------------------------Exit Site Evaluation	Findings--------------------------
	String[] Exit_Findings = ExitSite_Findings.split(",")
	for(int i=0;i<Exit_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('ExitSite')+Exit_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)
	}
	
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdCatheterLabel'), null, 'scrolltoelement')
	
//-------------------------- Tunnel Evaluation Findings --------------------------
	String[] Tunnel_Findings = TunnelEval_Findings.split(",")
	for(int i=0;i<Tunnel_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TunnelEval')+Tunnel_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)
	}
	
//----------------------------PD Catheter Evaluation Findings--------------------------------
	
	String[] PD_Findings = PDEval_Findings.split(",")
	for(int i=0;i<PD_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('PDEval')+PD_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)
	}
	
	WebUI.takeScreenshot()
	
	def nextBtn = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('next'))
	if(WebUI.waitForElementPresent(nextBtn, 2, FailureHandling.OPTIONAL)) {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), '', 'click')
		WebUI.delay(2)
		
		String homeIntevention = excelData.getValue("HomeIntervention", 4)
		List<WebElement> ele = driver.findElements(By.xpath(prop.getProperty('intervention')+homeIntevention+prop.getProperty('brace')))
		println ele.size()
		for(WebElement intervention : ele) {
			intervention.click()
			WebUI.delay(1)
		}
		WebUI.takeScreenshot()
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}else {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdHomeDropDown'), '', 'click')
	WebUI.delay(1)
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccessEval'), '', 'click')
	WebUI.delay(2)
	
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccessEval'), '', 'click')
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdHomeDropDown'), '', 'click')
}








