import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')

WebUI.callTestCase(findTestCase('Test Cases/HomeEvaluations/PatientSelection'), [:], FailureHandling.OPTIONAL)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('AccessDetails'), '', 'click')
WebUI.delay(2)

WebUI.callTestCase(findTestCase('Test Cases/Add Access/AVF Access/AVF(Active In Use)'), [:], FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('Test Cases/Add Access/AVG Access/AVG(Temporarily Unusable)'), [:], FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('Test Cases/Add Access/CVC Access/CVC(Removed)'), [:], FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('Test Cases/Add Access/PD Access/PD(Waiting For Removal)'), [:], FailureHandling.OPTIONAL)
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('HomeEvalBtn'), '', 'click')
WebUI.delay(2)
WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver

Modality = driver.findElement(By.xpath(prop.getProperty('Modality')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Modality)

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('HomeView'),'Last 180 Days', prop.getProperty('HomeViewBtn'))
WebUI.takeScreenshot()

//------------Check OpenAll/CloseAll Functionality----------------------
OpenAll = driver.findElement(By.xpath(prop.getProperty('OpenAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",OpenAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('OpenAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CloseAll = driver.findElement(By.xpath(prop.getProperty('CloseAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",CloseAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('CloseAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

int size = CustomKeywords.'reusablekeywords.ReusableKeywords.getSize'(prop.getProperty('HomeAccess'))

for(int i=1;i<=size;i++) {
	String str = prop.getProperty('HomeAccess')+"["+i+"]"
	String Access1 = CustomKeywords.'reusablekeywords.ReusableKeywords.getText'(str)
	StringBuffer AccessText1 = new StringBuffer(Access1)
	println(AccessText1)
	if(AccessText1.indexOf("AVGraft")>=0) {
		WebUI.callTestCase(findTestCase('HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/CommonTestCases/AVGAccessEvaluation'), [:], FailureHandling.OPTIONAL)
		
		WebUI.callTestCase(findTestCase('HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/CommonTestCases/AVGCannulationEvaluation'), [:], FailureHandling.OPTIONAL)
		
	}else if(AccessText1.indexOf("AVFistula")>=0) {
		WebUI.callTestCase(findTestCase('HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/CommonTestCases/AVFAccessEvaluation'), [:], FailureHandling.OPTIONAL)
		
		WebUI.callTestCase(findTestCase('HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/CommonTestCases/AVFMaturationEvaluation'), [:], FailureHandling.OPTIONAL)
		
		WebUI.callTestCase(findTestCase('HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/CommonTestCases/AVFCannulationEvaluation'), [:], FailureHandling.OPTIONAL)
		
	}else if(AccessText1.indexOf("CVCatheter")>=0) {
		//WebUI.callTestCase(findTestCase('HomeEvaluations/Alert-AVF(MaturingHealing),AVG(Active),CVC(WaitingForRemoval),PD(Inactive)/CommonTestCases/CVCAccessEvaluation'), [:], FailureHandling.OPTIONAL)
		println "CVC Removed Access Should not be available"
		WebUI.closeBrowser()
	}else if(AccessText1.indexOf("PDCatheter")>=0) {
		WebUI.callTestCase(findTestCase('HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/CommonTestCases/PDAccessEvaluation'), [:], FailureHandling.OPTIONAL)
		
	}
}

WebUI.takeScreenshot()

//------------------------------------ Access Plan ------------------------------------------
WebUI.callTestCase(findTestCase('HomeEvaluations/Access Plan'), [:], FailureHandling.OPTIONAL)

//---------------------------- Edit/Delete Home Evaluation Events ---------------------------
WebUI.callTestCase(findTestCase('HomeEvaluations/Events'), [:], FailureHandling.OPTIONAL)

//------------Check Events Tab OpenAll/CloseAll Functionality----------------------
OpenAll = driver.findElement(By.xpath(prop.getProperty('OpenAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",OpenAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('OpenAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CloseAll = driver.findElement(By.xpath(prop.getProperty('CloseAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",CloseAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('CloseAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()


WebUI.callTestCase(findTestCase('HomeEvaluations/Patient Audit'), [:], FailureHandling.OPTIONAL)





