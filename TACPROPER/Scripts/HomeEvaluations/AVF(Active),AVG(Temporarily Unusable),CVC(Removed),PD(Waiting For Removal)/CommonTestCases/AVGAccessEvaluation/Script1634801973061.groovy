import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys
import org.openqa.selenium.WebElement

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')

WebUI.delay(2)

TestObject testObj =  new TestObject()

WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver

//Modality = driver.findElement(By.xpath(prop.getProperty('Modality')))
//js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Modality)


//========================AVG Access Evaluation==============================
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
String AVG_Look = excelData.getValue("Look", 1)
String AVG_Listen = excelData.getValue("Listen", 1)
String AVG_Feel = excelData.getValue("Feel", 1)
String AVG_Last30D = excelData.getValue("Last 30 Days", 1)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgHomeDropDown'), '', 'click')
WebUI.delay(2)
println prop.getProperty('avgAccessEval')


//''''''''''Verify AVG Access Evaluation Disabled''''''''''''''''''''
String ArterialNeedle = excelData.getValue("ArterialNeedleSize", 1)
String VenousNeedle = excelData.getValue("VenousNeedleSize", 1)
String accEvaldisable = prop.getProperty('avgAccessEval')+prop.getProperty('GrayedoutPlus')

def accessEval = testObj.addProperty('xpath', ConditionType.EQUALS, accEvaldisable)
if(WebUI.waitForElementPresent(accessEval, 2, FailureHandling.OPTIONAL)) {
	println "AVG Access Evaluation Cannot be Performed"
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgAccessEval'), '', 'click')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgAccessEval'), '', 'click')
	
}else {
		
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgAccessEval'), '', 'click')
	WebUI.takeScreenshot()
	
	String accEvalEnable = prop.getProperty('avgAccessEval')+prop.getProperty('EnablePlus')
	CustomKeywords.'customLocators.FindElement.pageAction'(accEvalEnable, '', 'click')
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TeleHealth'), '', 'click')
	
	//---------------Look Findings-------------------
	String[] Look_Findings = AVG_Look.split(",")
	for(int i=0;i<Look_Findings.length;i++) {
		println Look_Findings[i]
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Look')+Look_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	//---------------Listen Findings----------------------------
	String[] Listen_Findings = AVG_Listen.split(",")
	for(int i=0;i<Listen_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Listen')+Listen_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), null, 'scrolltoelement')
	
	//---------------Feel Findings----------------------------
	String[] Feel_Findings = AVG_Feel.split(",")
	for(int i=0;i<Feel_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Feel')+Feel_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	//---------------Findings In Last 30 Days----------------------------
	String[] Last30D_Findings = AVG_Last30D.split(",")
	for(int i=0;i<Last30D_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Past_30Days')+Last30D_Findings[i]+prop.getProperty('EvalBrace'), '', 'click')
		WebUI.delay(1)
	}
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('ArterialNeedleSize'),ArterialNeedle, prop.getProperty('ArterialNeedleSizebtn'))
	WebUI.delay(1)
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('VenousNeedleSize'),VenousNeedle, prop.getProperty('VenousNeedleSizebtn'))
	
	WebUI.takeScreenshot()
	WebUI.delay(2)
	
	def nextBtn = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('next'))
	if(WebUI.waitForElementPresent(nextBtn, 2, FailureHandling.OPTIONAL)) {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), '', 'click')
		WebUI.delay(2)
		
		def alertPopup = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('NeedleAlertPopup'))
		if(WebUI.waitForElementPresent(alertPopup, 2, FailureHandling.OPTIONAL)) {
			CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('AccEvalPage'), null, 'scrolltoelement')
			WebUI.takeScreenshot()
			CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('AlertOk'), '', 'click')
			WebUI.delay(2)
		}
		
		String homeIntevention = excelData.getValue("HomeIntervention", 1)
		List<WebElement> ele = driver.findElements(By.xpath(prop.getProperty('intervention')+homeIntevention+prop.getProperty('brace')))
		
		println ele.size()
		for(WebElement intervention : ele) {
			intervention.click()
			WebUI.delay(1)
		}
		WebUI.takeScreenshot()
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}else {
		
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
		
	}
	
	
	
	WebUI.delay(2)
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgHomeDropDown'), '', 'click')
	WebUI.delay(2)
	println prop.getProperty('avgAccessEval')
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgAccessEval'), '', 'click')
	WebUI.delay(2)
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgAccessEval')+prop.getProperty('ViewEvaluation'), '', 'click')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'scrolltoelement')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'click')
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgAccessEval'), '', 'click')
}

