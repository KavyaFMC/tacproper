import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys
import org.openqa.selenium.WebElement

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')

//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('HomeEvalBtn'), '', 'click')
WebUI.delay(2)

TestObject testObj =  new TestObject()

WebDriver driver = DriverFactory.getWebDriver()

//========================AVG Cannulation Evaluation==============================
//Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
//String AVG_Look = excelData.getValue("Look", 1)
//String AVG_Listen = excelData.getValue("Listen", 1)
//String AVG_Feel = excelData.getValue("Feel", 1)
//String AVG_Last30D = excelData.getValue("Last 30 Days", 1)

//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgHomeDropDown'), '', 'click')
WebUI.delay(2)
println prop.getProperty('avgCannEval')



//''''''''''Verify AVG Access Evaluation Disabled''''''''''''''''''''
String cannEvaldisable = prop.getProperty('avgCannEval')+prop.getProperty('GrayedoutPlus')

def accessEval = testObj.addProperty('xpath', ConditionType.EQUALS, cannEvaldisable)
if(WebUI.waitForElementPresent(accessEval, 2, FailureHandling.OPTIONAL)) {
	println "AVG Cannulation Evaluation Cannot be Performed"
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgCannEval'), '', 'click')
	WebUI.delay(1)
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgCannEval'), '', 'click')
	WebUI.delay(2)
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgHomeDropDown'), '', 'click')
}else {
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgCannEval'), '', 'click')
	println "AVG Cannulation Evaluation is Not Required"
	//WebUI.closeBrowser()
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgCannEval'), '', 'click')
	WebUI.delay(1)
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('avgHomeDropDown'), '', 'click')
}

	

