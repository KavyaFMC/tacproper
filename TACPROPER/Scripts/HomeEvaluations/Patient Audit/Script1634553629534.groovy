import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//Audit.properties')

WebUI.waitForPageLoad(5)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PatientSelection', true)
def MRN=excelData.getValue("MRN", 1)
println(MRN)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('audit'), '', 'click')
WebUI.delay(5)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Audit_MRN'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Audit_MRN'),MRN, 'input')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('ActionType_Dropdown'), 'Task', prop.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('ActionType_Dropdown'), 'Home - Access Evaluation', prop.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('ActionType_Dropdown'), 'Event', prop.getProperty('ActionType'))
WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('closeaudit'), '', 'click')
WebUI.delay(3)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('print'), '', 'click')
WebUI.delay(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cancel'), null, 'scrolltoelement')
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cancel'), '', 'click')





