import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeEvents.properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('event'), '', 'click')

WebUI.waitForPageLoad(5)
Object avfExcelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVF', true)
Object avgExcelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
Object cvcExcelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
Object pdExcelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PD', true)


CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('viewinevents'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('clickview'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()
int size = CustomKeywords.'reusablekeywords.ReusableKeywords.getSize'(prop.getProperty('EventsEdit'))

for(int i=1;i<=size;i++) {
	String str = prop.getProperty('EventsEdit')+prop.getProperty('leftBrace')+i+prop.getProperty('rightBrace')
	String editEvent = CustomKeywords.'reusablekeywords.ReusableKeywords.getText'(str)
	StringBuffer eventText = new StringBuffer(editEvent)
//------------------------------Edit Home Event-------------------------------------	
	CustomKeywords.'customLocators.FindElement.pageAction'(str, null, 'scrolltoelement')
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('EventsEdit')+prop.getProperty('leftBrace')+i+prop.getProperty('pencilbutton'), '', 'click')
	WebUI.takeScreenshot()
	def editEvents
	def eventInterventions
	if(eventText.indexOf("AVFistula")>=0) {
		editEvents=avfExcelData.getValue("EditEvents", 1)
		println(editEvents)
		eventInterventions=avfExcelData.getValue("HomeIntervention", 1)
		println(eventInterventions)
			
	}else if(eventText.indexOf("AVGraft")>=0) {
		editEvents=avgExcelData.getValue("EditEvents", 1)
		println(editEvents)
		eventInterventions=avgExcelData.getValue("HomeIntervention", 1)
		println(eventInterventions)
			
	}else if(eventText.indexOf("CVCatheter")>=0) {
		editEvents=cvcExcelData.getValue("EditEvents", 1)
		println(editEvents)
		eventInterventions=cvcExcelData.getValue("HomeIntervention", 1)
		println(eventInterventions)
			
	}else if(eventText.indexOf("PDCatheter")>=0) {
		editEvents=pdExcelData.getValue("EditEvents", 1)
		println(editEvents)
		eventInterventions=pdExcelData.getValue("HomeIntervention", 1)
		println(eventInterventions)
			
	}
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('changetoerroneousXpath'),editEvents, prop.getProperty('editevent'))
	WebUI.delay(2)
	WebUI.takeScreenshot()
	
	WebUI.delay(2)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('addintervention'), '', 'click')
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('selectinterventionXpath'),eventInterventions, prop.getProperty('intervention'))
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('textarea'), 'Text123', 'input')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('saveintervention'), '', 'click')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('yes'), '', 'click')
	WebUI.delay(2)
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(str, null, 'scrolltoelement')
//--------------------------------Delete Home Event-----------------------------------
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('EventsEdit')+prop.getProperty('leftBrace')+i+prop.getProperty('deleteBtn'), '', 'click')
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('deleatenotes'), 'Notes123', 'input')
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('eventdeleate'), '', 'click')
	WebUI.delay(2)
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('audit'), null, 'scrolltoelement')
	WebUI.delay(2)
}



