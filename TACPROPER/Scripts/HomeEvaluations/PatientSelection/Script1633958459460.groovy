import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import helperUtilities.HelperFunctions as HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys

Properties prop = HelperFunctions.getTheObject('C:\\Katalon\\TACPROPER\\DataBase//HomeEvaluations//HomeLanding.properties')
Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

//WebUI.openBrowser('http://dama2.qa-intranet.fmcna.com/tac-app')
WebUI.openBrowser(GlobalVariable.URL)


WebUI.waitForPageLoad(5)

WebUI.maximizeWindow()

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx',
	'PatientSelection', true)

def Username = excelData.getValue('User', 1)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), Username , 'input')

//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('username'), GlobalVariable.Username , 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('password'), GlobalVariable.Password, 'input')

//CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), 'tnurse', 'input')
//CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), 'Today123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('signinbutton'), '', 'click')

WebUI.waitForPageLoad(5)

//Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx',
//	'PatientSelection', true)

def Clinic = excelData.getValue('Clinic', 1)

//def Clinic="1276"
println(Clinic)

//def MRN="5000609076"
def MRN = excelData.getValue('MRN', 1)

println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('selectclinic'), Clinic, 'input')

WebUI.delay(2)

Robot rb = new Robot()

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('selectpatient'), MRN, 'input')

WebUI.delay(3)
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('selectpatient1'), '', 'click')

WebUI.delay(3)

WebUI.takeScreenshot()
