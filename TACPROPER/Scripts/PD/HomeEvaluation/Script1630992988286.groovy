import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys
import org.openqa.selenium.WebElement

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//CommonFunctions//HomeEvaluation.properties')


CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('HomeEvalBtn'), '', 'click')
WebUI.delay(2)

TestObject testObj =  new TestObject()

WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PD', true)
String Peritonitis_Findings = excelData.getValue("Symptoms of Peritonitis", 1)
String ExitSite_Findings = excelData.getValue("Exit Site Evaluation", 1)
String TunnelEval_Findings = excelData.getValue("Tunnel Evaluation", 1)
String PDEval_Findings = excelData.getValue("PD Catheter Evaluation", 1)


Modality = driver.findElement(By.xpath(prop.getProperty('Modality')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Modality)

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('HomeView'),'Last 180 Days', prop.getProperty('HomeViewBtn'))
WebUI.delay(2)
WebUI.takeScreenshot()

//------------Check OpenAll/CloseAll Functionality----------------------
OpenAll = driver.findElement(By.xpath(prop.getProperty('OpenAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",OpenAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('OpenAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CloseAll = driver.findElement(By.xpath(prop.getProperty('CloseAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",CloseAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('CloseAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

//---------------------------- PD Access Evaluation ------------------------------------

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdHomeDropDown'), '', 'click')
WebUI.delay(1)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccess')+"Access Evaluation"+prop.getProperty('brace'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('GrayedoutPlus')+1+']')
if(WebUI.waitForElementPresent(a, 2, FailureHandling.OPTIONAL)) {
	println "Cannot Perform Access Evaluation"
}else {
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('EnablePlus'), '', 'click')
	WebUI.delay(3)
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('TeleHealth'), '', 'click')
	WebUI.delay(1)
	
//-------------------------Symptoms Of Peritonitis Findings-----------------------
	String[] Peri_Findings = Peritonitis_Findings.split(",")
	for(int i=0;i<Peri_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('findings')+Peri_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)	
	}
	
	
//-------------------------Exit Site Evaluation	Findings--------------------------
	String[] Exit_Findings = ExitSite_Findings.split(",")
	for(int i=0;i<Exit_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('findings')+Exit_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)
	}
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdCatheterLabel'), null, 'scrolltoelement')
	WebUI.takeScreenshot()
	
//-------------------------- Tunnel Evaluation Findings --------------------------
	String[] Tunnel_Findings = TunnelEval_Findings.split(",")
	for(int i=0;i<Tunnel_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('findings')+Tunnel_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)
	}
	
//----------------------------PD Catheter Evaluation Findings--------------------------------
	
	String[] PD_Findings = PDEval_Findings.split(",")
	for(int i=0;i<PD_Findings.length;i++) {
		CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('findings')+PD_Findings[i]+prop.getProperty('brace'), '', 'click')
		WebUI.delay(1)
	}
	
	WebUI.takeScreenshot()
	
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('next'), '', 'click')
	WebUI.delay(2)
	
	String homeIntevention = excelData.getValue("HomeIntervention", 1)
	List<WebElement> ele = driver.findElements(By.xpath(prop.getProperty('intervention')+homeIntevention+prop.getProperty('brace')))
	
	println ele.size()
	for(WebElement intervention : ele) {
		intervention.click()
		WebUI.delay(1)
	}
	WebUI.takeScreenshot()
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
}

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdHomeDropDown'), '', 'click')
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccess')+"Access Evaluation"+prop.getProperty('brace'), '', 'click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('pdAccess')+"Access Evaluation"+prop.getProperty('brace')+prop.getProperty('ViewEvaluation'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'scrolltoelement')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('close'), null, 'click')
WebUI.delay(2)
//------------------------ Edit Home Events ---------------------------------
Properties prop2 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\PD\\Event.Properties')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('viewinevents'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('clickview'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('pencilbutton'), '', 'click')

def editEvents=excelData.getValue("EditEvents", 1)
println(editEvents)
String homeIntevention = excelData.getValue("HomeIntervention", 1)
println(homeIntevention)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('changetoerroneousXpath'),editEvents, prop2.getProperty('editevent'))
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('addintervention'), '', 'click')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('selectinterventionXpath'),homeIntevention, prop2.getProperty('intervention'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('textarea'), 'Text123', 'input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('saveintervention'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('yes'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('deleate'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('deleatenotes'), 'Notes123', 'input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('eventdeleate'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

//------------Check OpenAll/CloseAll Functionality----------------------
OpenAll = driver.findElement(By.xpath(prop.getProperty('OpenAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",OpenAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('OpenAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CloseAll = driver.findElement(By.xpath(prop.getProperty('CloseAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",CloseAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('CloseAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()





