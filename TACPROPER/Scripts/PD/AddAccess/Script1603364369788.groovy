import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions
import java.text.SimpleDateFormat;
import java.text.Format;
import java.util.Date;

Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

Properties prop = HelperFunctions.getTheObject('C:\\Katalon\\TACPROPER\\DataBase\\PD\\AddAccess.Properties')
Properties prop1=HelperFunctions.getObj('C:\\katalon\\TACPROPER\\DataBase\\StaticValues\\Staticvalues.Properties')
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PD', true)
def accessName=excelData.getValue("AccessName", 1)
println(accessName)
def accesssubType=excelData.getValue("AccessSubType", 1)
println(accesssubType)
def siteandLocation=excelData.getValue("SiteandLocation", 1)
println(siteandLocation)
def accessStatus=excelData.getValue("AccessStatus", 1)
println(accessStatus)
def AccessStatusReason=excelData.getValue("AccessStatusReason", 1)
println(AccessStatusReason)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('PD'),accessName, prop.getProperty('addaccess'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accesssubtypeXpath'),accesssubType, prop.getProperty('accesssubtype'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('exitsiteXpath'), siteandLocation, prop.getProperty('exitsite'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('directionofexitsiteXpath'),prop1.getProperty('directionofexitsite'), prop.getProperty('directionofexitsite'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusXpath'),accessStatus,prop.getProperty('accessstatus'))
if (accessStatus=="Removed") {
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}
if (accessStatus=="Inactive (Usable, not used)") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))
	
}

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('statusdate'))

Date date = new Date();
SimpleDateFormat DateFor = new SimpleDateFormat("MM/dd/yyyy");
String CurrentDate= DateFor.format(date);
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('datecreated'), CurrentDate, 'input')

//CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('facilitytypeXpath'),prop1.getProperty('facilitytype'),prop.getProperty('facilitytype'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providerWhoPlacedAccessXpath'), prop1.getProperty('providerWhoPlacedAccess'), prop.getProperty('providerWhoPlacedAccess'))
/*CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('look'), prop1.getProperty('look'), 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('lookmenu'), prop1.getProperty('lookmenu'), 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('selectma'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('search'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('selectname'), '', 'click')
*/
//---------------------Select Provider LookUp-----------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('LastName'), 'Li', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('State'), 'AL', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Search'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('SelectLookUp'), '', 'click')

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providertypeXpath'),prop1.getProperty('providertype') , prop.getProperty('providertype'))
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providerphonenumber'), prop1.getProperty('providerphonenumber'), 'input')

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('catheterbrandnameXpath'), prop1.getProperty('catheterbrandname'), prop.getProperty('catheterbrandname'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('lotnumber'), prop1.getProperty('lotnumber'), 'input')

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('extensionsPresentXpath'), prop1.getProperty('extensionsPresent'), prop.getProperty('extensionsPresent'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('catheterBuriedXpath'), prop1.getProperty('catheterBuried'), prop.getProperty('catheterBuried'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('placementTechniqueXpath'), prop1.getProperty('placementTechnique'), prop.getProperty('placementTechnique'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('exitSiteSuturesXpath'), prop1.getProperty('exitSiteSutures'), prop.getProperty('exitSiteSutures'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('tipPlacementSiteXpath'), prop1.getProperty('tipPlacementSite'), prop.getProperty('tipPlacementSite'))

if (prop.getProperty('tipPlacementSiteDescription')) {
	CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('tipPlacementSiteDescription'),prop1.getProperty('tipPlacementSiteDescription'), 'input')
}

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('connectortypeXpath'), prop1.getProperty('connectortype') , prop.getProperty('connectortype'))

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('initialDressingChangeDate'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('initialDressingChangePerformerXpath'),  prop1.getProperty('initialDressingChangePerformer'), prop.getProperty('initialDressingChangePerformer'))

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('otherProcedurePerformed'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('otherProcedurePerformedXpath'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('surgeonnotes'), 'Notes@123', 'input')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
//------------------Capturing Error Message---------------------
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
WebUI.delay(2)
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')

//if (prop.getProperty("ActiveOKButton")) {
	
	//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('ActiveOKButton'), '', 'click')
//}

WebUI.delay(2)

WebUI.takeScreenshot()


