import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import reusableComponents1.DatepickerHandler
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions

Properties prop1 = HelperFunctions.getTheObject('C:\\Users\\z00485076\\Katalon Studio\\TACPROPER\\DataBase\\CVC\\PatientEducation.Properties')
prop0 = HelperFunctions.getTheObj('C:\\Users\\z00485076\\Katalon Studio\\TACPROPER\\DataBase\\Environment.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('addeducationtopic'), '','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patientaccessplan'), '','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('connectedaccessplan'), '','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('topic'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('selecttopic'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('materialprovided'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationedit'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('topic'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('selecttopic2'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationdeleate'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patientyes'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)




