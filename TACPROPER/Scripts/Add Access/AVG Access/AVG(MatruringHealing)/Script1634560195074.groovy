import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import helperUtilities.HelperFunctions as HelperFunctions
import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

Properties prop = HelperFunctions.getTheObject('C:\\Katalon\\TACPROPER\\DataBase\\AVG\\AddAccess.Properties')
Properties prop2=HelperFunctions.getObj('C:\\katalon\\TACPROPER\\DataBase\\StaticValues\\Staticvalues.Properties')
WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
def accessName=excelData.getValue("AccessName", 2)
println(accessName)
def accesssubtype=excelData.getValue("AccessSubType", 2)
println(accesssubtype)
def siteandlocation=excelData.getValue("SiteandLocation", 2)
println(siteandlocation)
def accessstatus=excelData.getValue("AccessStatus", 2)
println(accessstatus)
def AccessStatusReason=excelData.getValue("AccessStatusReason", 2)
println(AccessStatusReason)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('avg'),accessName, prop.getProperty('addaccess'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accesssubtypeXpath'),accesssubtype, prop.getProperty('accesssubtype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('sitelocationXpath'),siteandlocation, prop.getProperty('sitelocation'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusXpath'),accessstatus, prop.getProperty('accessstatus'))

if (accessstatus=="Maturing/Healing") {
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}
if (accessstatus=="Temporarily Unusable") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))
	
}

if (accessstatus=="Permanently Unusable") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('statusdate'))
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('anatomicalpositionXpath'),prop2.getProperty("anatomicalpositionAVG") ,prop.getProperty('anatomicalposition'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('facilitytypeXpath'),prop2.getProperty("facilitytypeAVG"),prop.getProperty('facilitytype'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providerXpath'),prop2.getProperty("providerAVG"), prop.getProperty('provider'))
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providerName'),prop2.getProperty("providerNameAVG"), 'input')
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providernumber'),prop2.getProperty("providernumberAVG"), 'input')

//---------------------Select Provider LookUp-----------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('LastName'), 'Li', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('State'), 'AL', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Search'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('SelectLookUp'), '', 'click')

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providertypeXpath'),prop2.getProperty("providertypeAVG"), prop.getProperty('providertype'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('lotnumber'), prop2.getProperty("lotnumberAVG"), 'input')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('configurationXpath'), prop2.getProperty("configurationAVG"), prop.getProperty('configuration'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('directionXpath'), prop2.getProperty("directionAVG"), prop.getProperty('direction'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('surgeonnotes'), 'Text', 'input')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
