import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions

Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

Properties prop = HelperFunctions.getTheObject('C:\\Katalon\\TACPROPER\\DataBase\\CVC\\AddAccess.Properties')
Properties prop1=HelperFunctions.getObj('C:\\katalon\\TACPROPER\\DataBase\\StaticValues\\Staticvalues.Properties')
WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
def accessName=excelData.getValue("AccessName", 3)
println(accessName)
def accesssubtype=excelData.getValue("AccessSubType", 3)
println(accesssubtype)
def siteandlocation=excelData.getValue("SiteandLocation", 3)
println(siteandlocation)
def accessstatus=excelData.getValue("AccessStatus", 3)
println(accessstatus)
def AccessStatusReason=excelData.getValue("AccessStatusReason", 3)
println(AccessStatusReason)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('cvc'),accessName, prop.getProperty('addaccess'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accesssubtypeXpath'),accesssubtype, prop.getProperty('accesssubtype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('sitelocationXpath'),siteandlocation, prop.getProperty('sitelocation'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusXpath'),accessstatus, prop.getProperty('accessstatus'))

if (accessstatus=="Removed") {
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('statusdate'))
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('anatomicalpositionXpath'), prop1.getProperty('anatomicalpositionCVC'),prop.getProperty('anatomicalposition'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('facilitytypeXpath'),prop1.getProperty('facilitytypeCVC'),prop.getProperty('facilitytype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providerXpath'),prop1.getProperty('providerCVC'), prop.getProperty('provider'))
WebUI.delay(2)
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providerName'),prop1.getProperty('providerNameCVC'), 'input')
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providernumber'),prop1.getProperty('providernumberCVC'), 'input')
//---------------------Select Provider LookUp-----------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('LastName'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('LastName'), 'Li', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('State'), 'AL', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Search'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('SelectLookUp'), '', 'click')

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providertypeXpath'),prop1.getProperty('providertypeCVC'), prop.getProperty('providertype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('longtermXpath'), prop1.getProperty('longtermCVC'), prop.getProperty('longterm'))
//-------------------------Highlight Reason For Catheter Form Link----------------------------
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('reasonforcatheterXpath'), prop1.getProperty('reasonforcatheterCVC'), prop.getProperty('reasonforcatheter'))
WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver

WebElement ReasonLink = driver.findElement(By.xpath(prop.getProperty('ReasonAccessFormLink')))
String FormLink = ReasonLink.getAttribute('href')

if(FormLink.equals(prop1.getProperty('PatientRefusedReasonLink'))) {
	link = driver.findElement(By.xpath(prop.getProperty('ReasonAccessFormLink')))
	js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 3px solid red;')",link)
	WebUI.takeScreenshot()
}else {
	println 'Invalid Link'
}

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('brandnameXpath'),prop1.getProperty('brandnameCVC'), prop.getProperty('brandname'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('lotnumber'),prop1.getProperty('lotnumberCVC'), 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('fillVolumevenouslumen'),prop1.getProperty('fillVolumevenouslumenCVC'), 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('fillvolumearteriallumen'),prop1.getProperty('fillvolumearteriallumenCVC'), 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('surgeonnotes'),prop1.getProperty('surgeonnotesCVC'), 'input')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')

//if (prop.getProperty("ActiveOKButton")) {
	
	//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('ActiveOKButton'), '', 'click')
//}
WebUI.delay(2)
WebUI.takeScreenshot()
