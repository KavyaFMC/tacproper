import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import helperUtilities.HelperFunctions as HelperFunctions

import java.text.SimpleDateFormat;
import java.text.Format;
import java.util.Date;

Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

Properties prop = HelperFunctions.getTheObject('C:\\Katalon\\TACPROPER\\DataBase\\AVG\\AddAccess.Properties')
Properties prop2=HelperFunctions.getObj('C:\\katalon\\TACPROPER\\DataBase\\StaticValues\\Staticvalues.Properties')
WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
def accessName=excelData.getValue("AccessName", 1)
println(accessName)
def accesssubtype=excelData.getValue("AccessSubType", 1)
println(accesssubtype)
def siteandlocation=excelData.getValue("SiteandLocation", 1)
println(siteandlocation)
def accessstatus=excelData.getValue("AccessStatus", 1)
println(accessstatus)
def AccessStatusReason=excelData.getValue("AccessStatusReason", 1)
println(AccessStatusReason)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('avg'),accessName, prop.getProperty('addaccess'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accesssubtypeXpath'),accesssubtype, prop.getProperty('accesssubtype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('sitelocationXpath'),siteandlocation, prop.getProperty('sitelocation'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusXpath'),accessstatus, prop.getProperty('accessstatus'))

if (accessstatus=="Maturing/Healing") {
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}
if (accessstatus=="Temporarily Unusable") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))
	
}

if (accessstatus=="Permanently Unusable") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('statusdate'))

Date date = new Date();
SimpleDateFormat DateFor = new SimpleDateFormat("MM/dd/yyyy");
String CurrentDate= DateFor.format(date);
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('datecreated'), CurrentDate, 'input')

//CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('anatomicalpositionXpath'),prop2.getProperty("anatomicalpositionAVG") ,prop.getProperty('anatomicalposition'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('facilitytypeXpath'),prop2.getProperty("facilitytypeAVG"),prop.getProperty('facilitytype'))

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providerXpath'),prop2.getProperty("providerAVG"), prop.getProperty('provider'))
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providerName'),prop2.getProperty("providerNameAVG"), 'input')
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('providernumber'),prop2.getProperty("providernumberAVG"), 'input')

//---------------------Select Provider LookUp-----------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('LastName'), 'Li', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('State'), 'AL', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Search'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('SelectLookUp'), '', 'click')

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providertypeXpath'),prop2.getProperty("providertypeAVG"), prop.getProperty('providertype'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('lotnumber'), prop2.getProperty("lotnumberAVG"), 'input')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('configurationXpath'), prop2.getProperty("configurationAVG"), prop.getProperty('configuration'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('directionXpath'), prop2.getProperty("directionAVG"), prop.getProperty('direction'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('surgeonnotes'), 'Text', 'input')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
//------------------Capturing Error Message---------------------
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
WebUI.delay(2)
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
WebUI.delay(2)
//if (prop.getProperty("ActiveOKButton")) {
	
	//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('ActiveOKButton'), '', 'click')
//}

WebUI.delay(2)

WebUI.takeScreenshot()

'AFTBaseline'
WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit1'), '', 'click')

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('baselinevalue'), '555', 'input')
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('baselinedate'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.delay(1)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save1'), null, 'click')

'Edit AFTBaseline'
WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit1'), '', 'click')

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('baselinevalue'), '666', 'input')

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('baselinedate'))

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save1'), null, 'click')

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('finalsc'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('finalsc1'), '', 'click')

WebUI.takeScreenshot()

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('finalclose'), '', 'click')

WebUI.delay(2)

