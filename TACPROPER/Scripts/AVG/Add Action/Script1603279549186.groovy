import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions

Properties prop = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\PatientEducation.Properties')
WebUI.waitForPageLoad(5)

//---------------------Patient Education------------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('addeducationtopic'), '','click')
WebUI.delay(2)
WebUI.takeScreenshot()
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patientaccessplan'), '','click')
//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('connectedaccessplan'), '','click')
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
def addPatientEducation=excelData.getValue("AddPatientEducation", 1)
println(addPatientEducation)
//--------------------Select Patient Education Topic---------------------
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('selecttopicXpath'), addPatientEducation, prop.getProperty('topic'))

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('materialprovided'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducationsave'),'','click')
WebUI.delay(2)

WebUI.takeScreenshot()
WebUI.delay(2)
//---------------------Add Events-------------------
Properties prop3 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Event.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('viewinevents'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('clickview'), '', 'click')
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('addnewevent'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('affectedAccess'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('selectaffectedAccess'), '', 'click')
def addEventsObserverdBy=excelData.getValue("AddEventsObserverdBy", 1)
println(addEventsObserverdBy)
def addEventsObserverdDuring=excelData.getValue("AddEventsObserverdDuring", 1)
println(addEventsObserverdDuring)
def findings=excelData.getValue("Findings", 1)
println(findings)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('observedByXpath'), addEventsObserverdBy, prop3.getProperty('observedBy'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('observedduringXpath'),addEventsObserverdDuring, prop3.getProperty('observedduring'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('checkboxdate'), '', 'click')
CustomKeywords.'customLocators.FindElement.SelectFindings'(prop3.getProperty('Findings'),findings)
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('movetoright'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('next'), '', 'click')
WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('next1'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('finding3'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('findingssave'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()
//------------------------ADD APPOINTMENT--------------------------------
Properties prop2=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Appointment.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentclick'),'','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('view'),'','click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('allinterventions'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('addappointment'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentaffectedaccess'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('selectavgappointment'),'','click')

def addAppoinmentProviderName=excelData.getValue("AddAppoinmentProviderName", 1)
println(addAppoinmentProviderName)
def addAppoinmentReasonForAppointment=excelData.getValue("AddAppoinmentReasonForAppointment", 1)
println(addAppoinmentReasonForAppointment)
def addAppoinmentType=excelData.getValue("AddAppoinmentType", 1)
println(addAppoinmentType)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('providerselectXpath'),addAppoinmentProviderName, prop2.getProperty('providerappointment'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('reasonforappointmentXpath'),addAppoinmentReasonForAppointment, prop2.getProperty('appointmentreason'))
//-----------------------Choose Appointment Type As Vessel Mapping--------------------------
CustomKeywords.'customLocators.FindElement.SelectFindings'(prop2.getProperty('choosethetype'),addAppoinmentType)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()


