import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions
//edit patient education
Properties prop = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\PatientEducation.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducationedit'),'','click')
WebUI.delay(2)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVG', true)
def editPatientEducation=excelData.getValue("EditPatientEducation", 1)
println(editPatientEducation)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('selecttopicXpath'),editPatientEducation, prop.getProperty('topic'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducationsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
//edit event

Properties prop3 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Event.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('viewinevents'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('clickview'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('pencilbutton'), '', 'click')
WebUI.takeScreenshot()
def editEvents=excelData.getValue("EditEvents", 1)
println(editEvents)
def eventInterventions=excelData.getValue("EventInterventions", 1)
println(eventInterventions)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('changetoerroneousXpath'),editEvents, prop3.getProperty('editevent'))
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('addintervention'), '', 'click')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('selectinterventionXpath'),eventInterventions, prop3.getProperty('intervention'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('textarea'), 'Text123', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('saveintervention'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('yes'), '', 'click')

WebUI.delay(2)

WebUI.takeScreenshot()

//EDIT APPOINTMENT
Properties prop2=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Appointment.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentclick'),'','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('view'),'','click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('allinterventions'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentedit'),'','click')
WebUI.delay(2)
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop2.getProperty('appointmentdate'))

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('hour'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('hourselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('min'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('minselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('pm'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('pmselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentnotes'),'notes123','input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('datesave'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('arrow'),'','click')
WebUI.delay(3)
WebUI.takeScreenshot()
WebUI.delay(2)
