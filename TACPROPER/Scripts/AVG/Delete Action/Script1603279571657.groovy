import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions

Properties prop = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\PatientEducation.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
//------------------------Delete Patient Education-----------------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patienteducationdeleate'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('patientyes'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
//---------------------Delete Events-------------------
Properties prop3 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Event.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('viewinevents'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('clickview'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('deleate'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('deleatenotes'), 'Notes123', 'input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('eventdeleate'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

//-------------------------DELETE APPOINTMENT------------------------
Properties prop2=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Appointment.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appointmentclick'),'','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('view'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('allinterventions'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('arrow'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appoitmentoutcome'),'','click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('appoitmentedit'),'','click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('apptcompleteXpath'), 'Completed as Scheduled', prop2.getProperty('apptstatus'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('procedureperformeddropdownXpath'), 'Yes', prop2.getProperty('procedureperformed'))
//---------------------Select Procedure Type-------------------------
//CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('proceduretypedropdownXpath'), ' PD Catheter Removal (49422)', prop2.getProperty('proceduretype'))

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('outcomenotes'),'Text123','input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('outcomesave'),'','click')
WebUI.delay(3)
WebUI.takeScreenshot()


