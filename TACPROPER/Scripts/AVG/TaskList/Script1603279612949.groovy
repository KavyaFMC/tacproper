import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions

Properties prop5 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Task.Properties')
Properties env = HelperFunctions.getTheObj('C:\\katalon\\TACPROPER\\DataBase\\Environment.Properties')

WebUI.waitForPageLoad(5)

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('threelines'), '', 'movetoelement')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('tasklist'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('addtasklist'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('addnotes'), 'Notes12345', 'input')

CustomKeywords.'reusablekeywords.ReusableKeywords.Futuredate'(prop5.getProperty('date'))
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('save'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('taskdescription'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('taskdescription'), 'Notes12345', 'input')

WebUI.waitForPageLoad(5)

WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('pencil'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('edittask'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('addnotes1'), 'Notes@123', 'input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('finalsave'), '', 'click')

WebUI.waitForPageLoad(5)

WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('pencil1'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('deleate'), '', 'click')

WebUI.waitForPageLoad(5)

WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('backtopatientcensus'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('clickpatientcensus'), '', 'click')

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PatientSelection', true)
def MRN=excelData.getValue("MRN", 1)
println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient'),MRN, 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient1'), '', 'click')

WebUI.takeScreenshot()