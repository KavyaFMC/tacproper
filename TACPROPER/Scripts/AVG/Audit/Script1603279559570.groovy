import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions

Properties prop5 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVG\\Task.Properties')
Properties env = HelperFunctions.getTheObj('C:\\katalon\\TACPROPER\\DataBase\\Environment.Properties')
WebUI.waitForPageLoad(5)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'PatientSelection', true)
def MRN=excelData.getValue("MRN", 1)
println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('audit'), '', 'click')
WebUI.delay(5)

CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Task', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('Audit_MRN'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('Audit_MRN'),MRN, 'input')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Access Details', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Access Status', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Access Flow Testing', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Patient Education', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Event', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop5.getProperty('ActionType_Dropdown'), 'Appointment', prop5.getProperty('ActionType'))
WebUI.delay(2)
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop5.getProperty('closeaudit'), '', 'click')
