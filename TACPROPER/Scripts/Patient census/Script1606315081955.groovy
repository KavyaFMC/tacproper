import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions
import internal.GlobalVariable

Properties prop2=HelperFunctions.getObj('C:\\katalon\\TACPROPER\\DataBase\\StaticValues\\Staticvalues.Properties')
Properties env  = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

//WebUI.openBrowser('http://dama2.qa-intranet.fmcna.com/tac-app')
WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(5)

WebUI.maximizeWindow()

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), GlobalVariable.Username , 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), GlobalVariable.Password, 'input')

//CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), 'tnurse', 'input')
//CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), 'Today123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('signinbutton'), '', 'click')

WebUI.waitForPageLoad(5)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVF', true)
def Clinic=excelData.getValue("Clinic", 1)
println(Clinic)
CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectclinic'),Clinic, 'input')
WebUI.delay(2)
Robot rb = new Robot()

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVF', true)
def MRN=excelData.getValue("MRN", 1)
println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient'),MRN, 'input')

WebUI.delay(3)

CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum1"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum2"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum3"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum4"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum5"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum6"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum7"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum8"))
CustomKeywords.'customLocators.FindElement.Patientcensus'(prop2.getProperty("coloum9"))
CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('clickarrow'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

