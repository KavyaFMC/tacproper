import org.aspectj.weaver.ast.Or

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By
import helperUtilities.HelperFunctions as HelperFunctions

import java.text.SimpleDateFormat;
import java.text.Format;
import java.util.Date;

Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')
Properties prop2=HelperFunctions.getObj('C:\\katalon\\TACPROPER\\DataBase\\StaticValues\\Staticvalues.Properties')
Properties prop = HelperFunctions.getTheObject('C:\\Katalon\\TACPROPER\\DataBase\\AVF\\AddAccess.Properties')

prop1 = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')
WebUI.delay(3)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'AVF', true)
def accessName=excelData.getValue("AccessName", 1)
def accesssubtype=excelData.getValue("AccessSubType", 1)
println(accessName)
def siteandlocation=excelData.getValue("SiteandLocation", 1)
println(siteandlocation)

def accessstatus=excelData.getValue("AccessStatus", 1)
println(accessstatus)

def AccessStatusReason=excelData.getValue("AccessStatusReason", 1)
println(AccessStatusReason)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('avf'),accessName, prop.getProperty('addaccess'))
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accesssubtypeXpath'),accesssubtype, prop.getProperty('accesssubtype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('sitelocationXpath'),siteandlocation, prop.getProperty('sitelocation'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusXpath'),accessstatus, prop.getProperty('accessstatus'))

WebDriver driver = DriverFactory.getWebDriver();
JavascriptExecutor js = (JavascriptExecutor) driver

if (accessstatus=="Maturing/Healing") {
	
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}
if (accessstatus=="Temporarily Unusable") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))
	TestObject testObj = new TestObject()
	def a = testObj.addProperty('xpath', ConditionType.EQUALS,  '//span[contains(text(),"Usable, Patient Refused")]')
	if(WebUI.waitForElementPresent(a, 2, FailureHandling.OPTIONAL)) {
		def Status_Reason = driver.findElement(By.xpath("//span[contains(text(),'Usable, Patient Refused')]"))
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Status_Reason)
	
		WebUI.takeScreenshot()
	}
}

if (accessstatus=="Permanently Unusable") {
	CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessstatusreasonXpath'),AccessStatusReason, prop.getProperty('accessstatusreason'))

}

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('statusdate'))

Date date = new Date();
SimpleDateFormat DateFor = new SimpleDateFormat("MM/dd/yyyy");
String CurrentDate= DateFor.format(date);
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('datecreated'), CurrentDate, 'input')

//CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('endovascularCreation'), null, 'click')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('anatomicalpositionXpath'),prop2.getProperty("anatomicalpositionAVF"),prop.getProperty('anatomicalposition'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('facilitytypeXpath'),prop2.getProperty("facilitytypeXpathAVF"),prop.getProperty('facilitytype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('providerXpath'),prop2.getProperty("providerAVF"), prop.getProperty('provider'))
WebUI.delay(2)
//---------------------Select Provider LookUp-----------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('LastName'), 'Li', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('State'), 'AL', 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Search'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('SelectLookUp'), '', 'click')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('ProviderTypeXpath'),prop2.getProperty("ProviderType"), prop.getProperty('providertype'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('patienthasvwingXpath'),prop2.getProperty("patienthasvwingAVF"), prop.getProperty('patienthasvwing'))

//-------------------------Update Depth Fields-----------------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('AccessDepthVenous'), prop2.getProperty("accessDepth"), 'input')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('AccessDiameter'), prop2.getProperty("accessDiameter"), 'input')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('SouceMappingXpath'),prop2.getProperty("sourceMapping"),prop.getProperty('SourceMapping'))

//CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('patienthasvwingXpath'),prop2.getProperty("patienthasvwingAVF"), prop.getProperty('patienthasvwing'))
//CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('venousXpath'),prop2.getProperty("venousAVF"), prop.getProperty('venous'))
//WebUI.delay(2)
//CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('depthXpath'),prop2.getProperty("depthAVF"), prop.getProperty('depth'))
//CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('accessdepthXpath'),prop2.getProperty("accessdepthAVF"),prop.getProperty('accessdepth'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('surgeonnotes'), 'Text', 'input')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
WebUI.delay(2)
//------------------Capturing Error Message---------------------
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
WebUI.delay(2)
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('datecreated'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save'), '', 'click')
WebUI.delay(5)
//if (prop.getProperty("ActiveOKButton")) {
	
	//CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('ActiveOKButton'), '', 'click')
//}
WebUI.delay(2)
WebUI.takeScreenshot()
'AFTBaseline'
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit1'), '', 'click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('baselinevalue'), '555', 'input')
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('baselinedate'))
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save1'), null, 'click')
'Edit AFTBaseline'
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('edit1'), '', 'click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('baselinevalue'), '666', 'input')
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop.getProperty('baselinedate'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('save1'), null, 'click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('finalsc'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('finalsc1'), '', 'click')
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('finalclose'), '', 'click')
WebUI.delay(2)

