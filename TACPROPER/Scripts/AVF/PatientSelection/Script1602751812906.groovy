import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import helperUtilities.HelperFunctions as HelperFunctions
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Properties prop = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\AVF\\Tac.Properties')
Properties env = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase\\Environment.Properties')

//WebUI.openBrowser('http://dama2.qa-intranet.fmcna.com/tac-app')
WebUI.openBrowser(GlobalVariable.URL)


WebUI.waitForPageLoad(5)

WebUI.maximizeWindow()

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx',
	'PatientSelection', true)

def Username = excelData.getValue('User', 1)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), Username , 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), GlobalVariable.Password, 'input')

//CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('username'), 'tnurse', 'input')
//CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('password'), 'Today123', 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('signinbutton'), '', 'click')

WebUI.waitForPageLoad(5)

//Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 
//    'PatientSelection', true)

def Clinic = excelData.getValue('Clinic', 1)

//def Clinic="1276"
println(Clinic)

//def MRN="5000609076"
def MRN = excelData.getValue('MRN', 1)

println(MRN)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectclinic'), Clinic, 'input')

WebUI.delay(2)

Robot rb = new Robot()

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient'), MRN, 'input')

WebUI.delay(3)
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(env.getProperty('selectpatient1'), '', 'click')

WebUI.delay(3)

WebUI.takeScreenshot()