import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions
import java.text.SimpleDateFormat;
import java.text.Format;
import java.util.Date;

Properties prop4 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\EditAccess.Properties')
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
def  editAccess=excelData.getValue(" EditAccess", 1)
println( editAccess)
CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('pencil'), null, 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('Edit_Access'), null, 'click')

Date date = new Date();
SimpleDateFormat DateFor = new SimpleDateFormat("MM/dd/yyyy");
String CurrentDate= DateFor.format(date);
CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('datecreated'), CurrentDate, 'input')

CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('editaccesssave'), null, 'click')
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('Button_Ok'), null, 'click')

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop4.getProperty('datecreated'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('editaccesssave'), null, 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('pencil'), null, 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('editaccessdetails'), null, 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('addnewstatus'), null, 'click')


CustomKeywords.'customLocators.FindElement.Selectelement'(prop4.getProperty('statuschangeXpath'), editAccess,prop4.getProperty('accessstatus'))

CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop4.getProperty('statusdate'))
WebUI.delay(1)

WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('editaccesssave'), null, 'click')
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop4.getProperty('yesbutton'), null, 'click')

WebUI.delay(3)

WebUI.takeScreenshot()


