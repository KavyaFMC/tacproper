import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions

Properties prop3=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Appointment.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('clickaccessplan'),'','click')
WebUI.delay(4)
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('viewinaccessplan'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('clickview'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('accessplanarrowclick'), '', 'click')
WebUI.delay(1)
WebUI.takeScreenshot()


