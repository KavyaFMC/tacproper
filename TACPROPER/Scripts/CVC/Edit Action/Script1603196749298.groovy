import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions

Properties prop1 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\PatientEducation.Properties')
prop0 = HelperFunctions.getTheObj('C:\\katalon\\TACPROPER\\DataBase\\Environment.Properties')
//edit patient education
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationedit'),'','click')
WebUI.delay(2)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
def editPatientEducation=excelData.getValue("EditPatientEducation", 1)
println(editPatientEducation)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop1.getProperty('selecttopicXpath'),editPatientEducation, prop1.getProperty('topic'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)

//edit events 

Properties prop2 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Events.Properties')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('viewinevents'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('clickview'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('pencilbutton'), '', 'click')
WebUI.takeScreenshot()
//Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
def editEvents=excelData.getValue("EditEvents", 1)
println(editEvents)
def eventInterventions=excelData.getValue("EventInterventions", 1)
println(eventInterventions)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('changetoerroneousXpath'),editEvents, prop2.getProperty('editevent'))
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('addintervention'), '', 'click')
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('selectinterventionXpath'),eventInterventions, prop2.getProperty('intervention'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('textarea'), 'Text123', 'input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('saveintervention'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('yes'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()


// edit appointment 
Properties prop3=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Appointment.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentclick'),'','click')
WebUI.waitForPageLoad(5)
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('view'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('allinterventions'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentedit'),'','click')
WebUI.delay(2)
CustomKeywords.'reusablekeywords.ReusableKeywords.Dateselection'(prop3.getProperty('appointmentdate'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('hour'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('hourselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('min'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('minselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('pm'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('pmselect'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentnotes'),'notes123','input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('datesave'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('arrow'),'','click')
WebUI.delay(3)
WebUI.takeScreenshot()
WebUI.delay(2)
