import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import helperUtilities.HelperFunctions
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriver as Keys

Properties prop = HelperFunctions.getTheObj('C:\\Katalon\\TACPROPER\\DataBase//CommonFunctions//HomeEvaluation.properties')


CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('HomeEvalBtn'), '', 'click')
WebUI.delay(2)

TestObject testObj =  new TestObject()

WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver

Modality = driver.findElement(By.xpath(prop.getProperty('Modality')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Modality)

CustomKeywords.'customLocators.FindElement.Selectelement'(prop.getProperty('HomeView'),'Last 180 Days', prop.getProperty('HomeViewBtn'))
WebUI.delay(2)
WebUI.takeScreenshot()

//------------Check OpenAll/CloseAll Functionality----------------------
OpenAll = driver.findElement(By.xpath(prop.getProperty('OpenAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",OpenAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('OpenAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CloseAll = driver.findElement(By.xpath(prop.getProperty('CloseAll')))
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",CloseAll)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('CloseAll'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

//---------------------------- CVC Access Evaluation -------------------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcHomeDropDown'), '', 'click')
WebUI.delay(1)

CustomKeywords.'customLocators.FindElement.pageAction'(prop.getProperty('cvcAccess')+"Access Evaluation"+prop.getProperty('brace'), '', 'click')

WebUI.delay(2)

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('GrayedoutPlus')+1+']')
if(WebUI.waitForElementPresent(a, 2, FailureHandling.OPTIONAL)) {
	println "Cannot Perform Access Evaluation"
}

WebUI.takeScreenshot()