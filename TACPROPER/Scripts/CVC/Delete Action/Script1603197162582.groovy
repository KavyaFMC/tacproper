import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as HelperFunctions
import helperUtilities.HelperFunctions as HelperFunctions

Properties prop1 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\PatientEducation.Properties')
prop0 = HelperFunctions.getTheObj('C:\\katalon\\TACPROPER\\DataBase\\Environment.Properties')
//--------------------Delete Patient Education----------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationdeleate'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patientyes'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()
WebUI.delay(2)
//------------------Delee Events-------------------

Properties prop2 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Events.Properties')
WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('viewinevents'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('clickview'), '', 'click')
WebUI.delay(1)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('deleate'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('deleatenotes'), 'Notes123', 'input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('eventdeleate'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

//------------------Delete Appointment-----------------------
Properties prop3=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Appointment.Properties')

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentclick'),'','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('view'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('allinterventions'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('arrow'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appoitmentoutcome'),'','click')
WebUI.delay(2)

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appoitmentedit'),'','click')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('apptcompleteXpath'), 'Completed as Scheduled', prop3.getProperty('apptstatus'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('procedureperformeddropdownXpath'), 'Yes', prop3.getProperty('procedureperformed'))
//-----------------------Select Procedure Type-------------------------
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('proceduretypedropdownXpath'), ' New CVCatheter placement (36558)', prop3.getProperty('proceduretype'))
WebUI.delay(2)
//-----------------------Select Specify New Access---------------------

TestObject testObj = new TestObject()
WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop3.getProperty('SpecifyAccessField'))
if(WebUI.verifyElementVisible(a, FailureHandling.OPTIONAL)) {
	CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('SpecifyNewAcces'),'','click')
	CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('SpecifyNewAccesDropdownXpath'),'','click')
	def access = driver.findElement(By.xpath(prop3.getProperty('SpecifyAccessField')))
	js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 3px solid red;')",access)
}
WebUI.takeScreenshot()

CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('outcomenotes'),'Text123','input')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('outcomesave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()


