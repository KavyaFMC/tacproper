import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helperUtilities.HelperFunctions as HelperFunctions

Properties prop1 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\PatientEducation.Properties')
prop0 = HelperFunctions.getTheObj('C:\\katalon\\TACPROPER\\DataBase\\Environment.Properties')
//-------------------------Add Patient Education-------------------------
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducation'), '','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('addeducationtopic'), '','click')
WebUI.delay(2)
//CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patientaccessplan'), '','click')
//CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('connectedaccessplan'), '','click')
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACPROPER//InputData//Katalon.TAC properData.xlsx', 'CVC', true)
def addPatientEducation=excelData.getValue("AddPatientEducation", 1)
println(addPatientEducation)
//----------------------Select Patient Education Topic---------------------
CustomKeywords.'customLocators.FindElement.Selectelement'(prop1.getProperty('selecttopicXpath'), addPatientEducation, prop1.getProperty('topic'))
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('materialprovided'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop1.getProperty('patienteducationsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()

//--------------------Add Event----------------------- 
Properties prop2 = HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Events.Properties')
WebUI.delay(2)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('event'), '', 'click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('viewinevents'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('clickview'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('addnewevent'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('affectedAccess'), '', 'click')

CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('selectaffectedAccess'), '', 'click')

def addEventsObserverdBy=excelData.getValue("AddEventsObserverdBy", 1)
println(addEventsObserverdBy)
def addEventsObserverdDuring=excelData.getValue("AddEventsObserverdDuring", 1)
println(addEventsObserverdDuring)
def findings=excelData.getValue("Findings", 1)
println(findings)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('observedByXpath'), addEventsObserverdBy, prop2.getProperty('observedBy'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop2.getProperty('observedduringXpath'),addEventsObserverdDuring, prop2.getProperty('observedduring'))
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('checkboxdate'), '', 'click')
CustomKeywords.'customLocators.FindElement.SelectFindings'(prop2.getProperty('Findings'),findings)
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('movetoright'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('next'), '', 'click')
WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('next1'), '', 'click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('finding3'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop2.getProperty('findingssave'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

//-------------------Add Appointment---------------------------
Properties prop3=HelperFunctions.getTheObject('C:\\katalon\\TACPROPER\\DataBase\\CVC\\Appointment.Properties')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentclick'),'','click')
WebUI.waitForPageLoad(5)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('view'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('allinterventions'),'','click')
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('addappointment'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentaffectedaccess'),'','click')
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('selectavfappointment'),'','click')

def addAppoinmentProviderName=excelData.getValue("AddAppoinmentProviderName", 1)
println(addAppoinmentProviderName)
def addAppoinmentReasonForAppointment=excelData.getValue("AddAppoinmentReasonForAppointment", 1)
println(addAppoinmentReasonForAppointment)
def addAppoinmentType=excelData.getValue("AddAppoinmentType", 1)
println(addAppoinmentType)
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('providerselectXpath'),addAppoinmentProviderName, prop3.getProperty('providerappointment'))
CustomKeywords.'customLocators.FindElement.Selectelement'(prop3.getProperty('reasonforappointmentXpath'),addAppoinmentReasonForAppointment, prop3.getProperty('appointmentreason'))
//--------------------Choose Appointment Type As PD Catheter Removal--------------------------
CustomKeywords.'customLocators.FindElement.SelectFindings'(prop3.getProperty('choosethetype'),addAppoinmentType)
WebUI.takeScreenshot()
CustomKeywords.'customLocators.FindElement.pageAction'(prop3.getProperty('appointmentsave'),'','click')
WebUI.delay(2)
WebUI.takeScreenshot()

