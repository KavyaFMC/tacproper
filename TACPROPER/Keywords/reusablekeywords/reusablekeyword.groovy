package reusablekeywords
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.YearMonth
import java.time.format.DateTimeFormatter

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class ReusableKeywords {

	@Keyword
	public void Dateselection(String xpath){

		YearMonth thisMonth = YearMonth.now()

		YearMonth lastMonth = thisMonth.minusMonths(1)

		//println lastMonth


		DateTimeFormatter abcx=DateTimeFormatter.ofPattern("MM")

		def finalmonth=lastMonth.format(abcx)

		println finalmonth


		DateFormat dateformat1=new SimpleDateFormat("dd")

		Date date= new Date()

		String todaydate=dateformat1.format(date)

		println todaydate
		DateFormat dateformat2=new SimpleDateFormat("yyyy")

		Date year= new Date()

		String Currentyear=dateformat2.format(year)
		String Lastyear=Currentyear.previous()
		println Lastyear

		if (finalmonth=="12"){

			println finalmonth+'/'+todaydate+'/'+Lastyear


			def DatePicker=finalmonth+'/'+todaydate+'/'+Lastyear

			println DatePicker
			TestObject testObj=new TestObject()

			testObj.addProperty("xpath", ConditionType.EQUALS, (xpath))

			WebUI.setText(testObj,DatePicker);


		}else{
			println finalmonth+'/'+todaydate+'/'+Currentyear


			def DatePicker=finalmonth+'/'+todaydate+'/'+Currentyear

			println DatePicker
			TestObject testObj=new TestObject()

			testObj.addProperty("xpath", ConditionType.EQUALS, (xpath))

			WebUI.setText(testObj,DatePicker);

		}


	}




	@Keyword
	public void Futuredate(String xpath){

		YearMonth thisMonth = YearMonth.now()

		YearMonth lastMonth = thisMonth.plusMonths(1)

		//println lastMonth



		DateTimeFormatter abcx=DateTimeFormatter.ofPattern("MM")

		def finalmonth=lastMonth.format(abcx)

		println finalmonth


		DateFormat dateformat1=new SimpleDateFormat("dd")

		Date date= new Date()

		String todaydate=dateformat1.format(date)

		println todaydate
		DateFormat dateformat2=new SimpleDateFormat("yyyy")

		Date year= new Date()

		String Currentyear=dateformat2.format(year)
		println Currentyear

		println finalmonth+'/'+todaydate+'/'+Currentyear


		def DatePicker=finalmonth+'/'+todaydate+'/'+Currentyear

		println DatePicker
		TestObject testObj=new TestObject()

		testObj.addProperty("xpath", ConditionType.EQUALS, (xpath))

		WebUI.setText(testObj,DatePicker);

	}


	@Keyword
	public String getText(String xpath) {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement ele = driver.findElement(By.xpath(xpath))
		String text = ele.getText()
		println text
		return text
	}
	
	@Keyword
	public int getSize(String xpath) {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement[] ele = driver.findElements(By.xpath(xpath))
		
		println ele.size()
		return ele.size()
	}
}


