package customLocators

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling

import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory



public class FindElement {

	@Keyword
	public void pageAction(String xpath,String value,String type){
		TestObject testObj=new TestObject()
		switch (type){

			case "input":
				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
				WebUI.setText(testObj,value);
				break;

			case "doubleclick":
				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
			//WebUI.waitForElementClickable(testObj, 5)
			//wait.until(ExpectedConditions.elementToBeClickable(testObj));
				WebUI.doubleClick(testObj)
				break;



			case "click":

				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
				WebUI.click(testObj);
				break;

			case "dropdown":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.selectOptionByLabel(testObj,value, false)
				break;
			case "verifyelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
			//WebUI.verifyElementPresent(testObj, 10)

				WebUI.verifyElementPresent(testObj, 5, FailureHandling.OPTIONAL)


				break;

			case "switchframe":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.switchToFrame(testObj, 3)
				break;

			case "defaultcontent":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.switchToDefaultContent()
				break;

			case "cleardata":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.clearText(testObj)
				break;

			case "verifytext":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.verifyElementText(testObj, value)
				break;

			case "movetoelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.mouseOver(testObj)
				break;

			case "scrolltoelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.scrollToElement(testObj, 10)
				break;

			case "verifyelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.waitForElementPresent(testObj, 5)
				break;

			case "takescreenshot":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.takeScreenshot()
				break;

			case "choosemenu":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.selectOptionByValue(testObj, value, false)
				break;

			case "valueselection":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.getText(testObj)
				break;

		}
	}
	@Keyword
	public void Selectelement(String xpath,String value,String Type){

		TestObject testObj=new TestObject()
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> options = driver.findElements(By.xpath((xpath)))
		//println((xpath))
		println(options.size())


		testObj.addProperty("xpath", ConditionType.EQUALS, (Type))
		WebUI.click(testObj);
		//WebUI.takeScreenshot()
		for(WebElement x :options){

			println('@'+x.getText())

			if(x.getText().equals((value))){
				x.click()
				break;
			}
		}
	}

	@Keyword
	public void AccessPlanValidation(String xpath,String value,String pass){

		TestObject testObj=new TestObject()

		testObj.addProperty("xpath", ConditionType.EQUALS, ('//a[text()="Access Plan"]'))
		WebUI.click(testObj);
		WebUI.delay(2)
		testObj.addProperty("xpath", ConditionType.EQUALS, ('//button[@id="statusPlan"]'))
		WebUI.click(testObj);
		testObj.addProperty("xpath", ConditionType.EQUALS, ('//a[@id="statusPlan-AllPlans"]'))
		WebUI.click(testObj);
		testObj.addProperty("xpath", ConditionType.EQUALS, ('//i[@class="pull-left glyphicon glyphicon-chevron-right"]'))
		WebUI.click(testObj);
		WebUI.delay(1)

		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> options = driver.findElements(By.xpath(xpath))
		println(options.size())

		for (WebElement x: options) {
			
			println ('@'+x.getText())
			if (x.getText().equals(value)) {
				println(pass)
				break
			}
			println('No findings added in Accessplan')
		}
	}


	@Keyword
	public void SelectFindings(String xpath,String value){

		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> options = driver.findElements(By.xpath(xpath))
		println(options.size())

		for(WebElement x :options){

			println('@'+x.getText())

			if(x.getText().equals(value)){
				x.click()
				break
			}
		}
	}

	@Keyword
	public void Patientcensus(String value){
		TestObject testObj=new TestObject()
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> options = driver.findElements(By.xpath('//span[@class="ui-grid-header-cell-label"]'))
		//println((xpath))
		println(options.size())
		int totalsize=options.size()
		if (totalsize == 9) {

			for(WebElement x :options){
				print x.getText()

				if (x.getText().equals(value)) {

					println value + "Label is present in Patient Census  page"
					break

				}

			}

		}else{
			println "New field added"

		}

	}
}












