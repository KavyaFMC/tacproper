<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CVC FINAL</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7c433043-983a-4601-a548-c27f724a0f32</testSuiteGuid>
   <testCaseLink>
      <guid>24b2e00a-412a-415a-95fd-b16f6de6a9cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/PatientSelection</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bd546e77-ab19-469f-ad67-815c6fd80ee4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>78c2479d-e41b-4bfe-9c2d-2ad3ff5bf84f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>05a26a69-3e16-4e94-bb29-99030e0bf563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CommonFunctions/PatientInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b1a328b-41f0-4a6d-92a7-3daa0a0be4d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/AddAccess</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>723de4d9-818b-45dd-9b6b-5f8c2b09cc1c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>31e09341-63c9-41c9-881e-b9607436e54c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2de309e1-6080-4f0e-85ad-95fd072e7a37</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5db8b579-c179-4658-963d-d06c6426c113</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7764eda7-d0c6-4cd4-92ed-0098c1d426b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Access plan Add,Edit,Deleate validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6744121d-aabc-42a9-aad5-199976f332d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Add Action</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f20cf58a-372b-485c-bef7-7f633584429a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d4f043f9-a8df-4831-b661-43591455ebeb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3797940e-1537-4962-b455-5d8454f76d9c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f359e24e-9048-447b-a0ce-a6e734af316d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1fcf9c84-fa25-4cb3-979f-6311f06183b2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>465f8478-0c6b-4db8-8a22-1158457fa5d0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8ad3b725-8bb0-4efc-9d01-b2748e9f9666</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>144c4b75-6471-47b2-9e35-41ee82d1bc7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Access plan Add,Edit,Deleate validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36af3e1d-b7d8-487b-bdbe-202ac170c5cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Edit Action</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f0b849c5-2de3-4191-bc4d-d79d45e2c09b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79b05dd1-b34e-4e88-bd3a-8274722278e1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>920ce523-6bcf-4319-8e63-483931482b55</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aced3a87-4b6e-4156-8436-2b9cdcd4a207</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Access plan Add,Edit,Deleate validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b51fdfbc-eb9c-4aa0-982a-2ae5d3056012</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Delete Action</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>764e6eb9-b80c-4537-99b3-f885f638db0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Access plan Add,Edit,Deleate validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e865cbcf-63c0-4c52-ae9b-df3f60562135</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/HomeEvaluation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0695975-cdb5-4eda-aed4-fe183b561c07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/TaskList</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>436e736a-a092-42e8-969d-0880e2df01c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/EditAccess</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c9db8601-8c95-41d4-9928-ede98791d076</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7388a200-af10-4f8b-b6a7-1a117844b890</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CVC/Audit</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6abc0651-04d8-4846-9e2c-048a40c8b367</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
