<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>HOMEEVALUATION</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c4c0778a-649b-477a-b603-301e2c3fc38c</testSuiteGuid>
   <testCaseLink>
      <guid>b3c7ab12-0be6-474a-b789-755e278150ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HomeEvaluations/Alert-AVF(MaturingHealing),AVG(Active),CVC(WaitingForRemoval),PD(Inactive)/Alert Final AVF(MaturingHealing),AVG(Active),CVC(WaitingForRemoval),PD(Inactive)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fc85e0e-56d7-4a9c-acaa-81e48e0720d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HomeEvaluations/AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)/Final AVF(Active),AVG(Temporarily Unusable),CVC(Removed),PD(Waiting For Removal)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96ac9f7e-51fb-435c-851f-8ce898760f29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HomeEvaluations/AVF(TemporarilyUnusable),AVG(MaturingHealing),CVC(Active),PD(ActiveFlushing)/Final AVF(TemporarilyUnusable),AVG(MaturingHealing),CVC(Active),PD(ActiveFlushing)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
