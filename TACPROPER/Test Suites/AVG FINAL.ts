<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AVG FINAL</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f422ce35-a40d-48bb-88c9-08b4b76f0ace</testSuiteGuid>
   <testCaseLink>
      <guid>ed4fece5-8a64-4c7e-9429-01b6c65e5611</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/PatientSelection</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>269e04be-9ede-45ec-908a-97cec4e26bd8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>754be031-b9d9-4a9f-8579-f63f369536aa</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>92916ac1-abb0-42bf-81b3-7352e7f49492</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CommonFunctions/PatientInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19de8221-917c-4f13-8765-5091be5e3226</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/AddAccess</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9c99b614-aed5-4939-af81-9bf8aea59f2e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>38385659-50be-469f-9760-3b16c6e87bef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>62b6175b-71f6-4509-83a5-61e2883f4e75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a4094b84-8fe6-4d50-a82a-ef80ab622998</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a6baf4a6-d988-4302-a129-fd2593b7e4f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Access plan Add,Edit,Delete validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>138e73cb-2dfd-461c-bd49-b6ba5c9fb781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Add Action</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba3948b8-ec2a-46f8-be57-a2d3a83f7a25</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7b65724d-65e6-4695-ba28-d3c8baa8aca6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5593fd78-d515-4ba3-8801-d66f8218165b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>43ef2fe6-b4f4-4c93-838f-68809c196e0c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fb9795ba-ea58-49b1-a9f2-08027cc88bda</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>acdebf78-f2b9-4f73-9878-f618d6e522c7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1dbe26c6-ea77-45d8-bf1c-f6cf6ac9859d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a5ab9c5d-c6bd-4c96-928e-c8178ea8a1e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Access plan Add,Edit,Delete validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e449a5e-a26f-4f4a-9284-ac0073672819</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Edit Action</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>410e6052-4c1f-43b7-8dba-359293215c36</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ab38cfcd-e224-47e5-8183-23a8ab499063</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d6fd3479-3cc3-4860-b473-226c5fafcd59</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cb2cf14b-cf61-47de-bea0-59507cf28caf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Access plan Add,Edit,Delete validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c616485-3ec6-42f1-b783-a100fc676456</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Delete Action</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1afd83c0-a1fe-41db-a585-4612714f17ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Access plan Add,Edit,Delete validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32adfd5a-e8e3-48d5-8b79-386312b75fa0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/HomeEvaluation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9e6bb83-7700-4200-a0cf-f0b44c0783d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/TaskList</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61930ebd-f5e1-4f79-a3dd-fb8c123c4b3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/EditAccess</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c83799f6-0d2b-4126-88c7-1c67070aed5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b14a1382-2af8-4fd4-89d2-307b09b6daba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AVG/Audit</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cca74f27-2bc1-4822-9214-a3abe93fc314</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
