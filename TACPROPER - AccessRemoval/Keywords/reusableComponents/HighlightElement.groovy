package reusableComponents

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver

import org.openqa.selenium.WebElement

import org.testng.Assert
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.kms.katalon.core.webui.common.WebUiCommonHelper

import com.kms.katalon.core.webui.driver.DriverFactory

import internal.GlobalVariable

public class HighlightElement {
	public static void main(String[] args) {
		//getting current date and time using Date class
		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date dateobj = new Date();
		System.out.println(df.format(dateobj));
		/*getting current date time using calendar class
		 * An Alternative of above*/
		Calendar calobj = Calendar.getInstance();
		System.out.println(df.format(calobj.getTime()));
	}
}
public class todayDate {

	@Keyword


	def today() {

		DateFormat dateFormat = new SimpleDateFormat(“dd/mm/yyyy”);

		Date date = new Date();

		String today = dateFormat.format(date);

		return today
	}
}



